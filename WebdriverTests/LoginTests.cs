using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;
using WebdriverClassLib.Pages;

namespace WebdriverTests
{
    public class LoginTests
    {
        StartPage startPage;
        HomePage homePage;
        IWebDriver driver;
        const string email1 = "senderMailTesting@proton.me", password1 = ":bi9j3A_bXUgAvs";

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();

            startPage = new StartPage(driver);
            homePage = new HomePage(driver);
        }

        [Test]
        public void CorrectLoginPasswordPair()
        {
            driver.Url = "https://proton.me/";
            bool result = startPage.Login(email1, password1);
            Assert.AreEqual(true, result && homePage.IsLoaded());
            homePage.LogOut();
            driver.Quit();
        }

        [Test]
        public void WrongLoginPasswordPair()
        {
            driver.Url = "https://proton.me/";
            bool result = startPage.Login(email1, "11111");
            Assert.AreEqual(false, result && homePage.IsLoaded());
            driver.Quit();
        }
    }
}