﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClassLib.Pages;
using WebdriverClassLib;
using System.Threading;

namespace WebdriverTests
{
    internal class SendLetterTests
    {
        StartPage startPage;
        HomePage homePage;
        EmailContents emailContents;
        IWebDriver driver;
        const string email1 = "senderMailTesting@proton.me", password1 = ":bi9j3A_bXUgAvs";
        const string email2 = "receiverMailTesting@proton.me", password2 = "ux3yRGkVc_!xqPd";

        const string messageSent = "4JrCHYMhTT6rK4n10aXFPxJ1zdk4d0FmG0g31BjNeugo3Tmw1Qnt6WSz6R7BvdBO6xJ94oCXCgdEgjbSJz62u9YLO0ZIaqJM1cDD";
        const string messageToReply = "NewNickname";

        const string expectedSender = "senderMailTesting";

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();

            startPage = new StartPage(driver);
            homePage = new HomePage(driver);
            emailContents = new EmailContents(driver);
        }

        [Test, Order(1)]
        public void EmailWasSent()
        {
            driver.Url = "https://proton.me/";
            startPage.Login(email1, password1);

            bool wasSent = homePage.SendEmail(email2, "Theme", messageSent);
            homePage.LogOut();

            Assert.IsTrue(wasSent);

            driver.Quit();
        }

        [Test, Order(2)]
        public void EmailWasReceived()
        {
            driver.Url = "https://proton.me/";
            startPage.Login(email2, password2);

            bool emailExists = homePage.OpenLastUnread();

            Assert.IsTrue(emailExists);

            driver.Quit();
        }

        [Test, Order(3)]
        public void ContentIsCorrect()
        {
            driver.Url = "https://proton.me/";
            startPage.Login(email2, password2);

            homePage.OpenLastUnread();

            string actualContent = emailContents.GetContent();
            Assert.AreEqual(messageSent, actualContent);

            driver.Quit();
        }

        [Test, Order(4)]
        public void SenderIsCorrect()
        {
            driver.Url = "https://proton.me/";
            startPage.Login(email2, password2);

            homePage.OpenLastUnread();

            string actualSender = emailContents.GetSender();
            Assert.AreEqual(expectedSender, actualSender);

            driver.Quit();
        }

        [Test, Order(5)]
        public void ReplyWasSuccessful()
        {
            driver.Url = "https://proton.me/";
            startPage.Login(email2, password2);

            homePage.OpenLastUnread();

            bool wasReplied = emailContents.SendReply(messageToReply);

            Assert.IsTrue(wasReplied);

            driver.Quit();
        }
    }
}
