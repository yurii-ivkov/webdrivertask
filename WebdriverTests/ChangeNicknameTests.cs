﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClassLib.Pages;
using System.Drawing;

namespace WebdriverTests
{
    internal class ChangeNicknameTests
    {
        StartPage startPage;
        HomePage homePage;
        IWebDriver driver;
        const string email1 = "senderMailTesting@proton.me", password1 = ":bi9j3A_bXUgAvs";

        const string newNickame = "NewNickname";

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();

            startPage = new StartPage(driver);
            homePage = new HomePage(driver);
        }

        [Test, Order(1)]
        public void ChangeNicknameWasFinished()
        {
            driver.Url = "https://proton.me/";
            startPage.Login(email1, password1);

            homePage.OpenLastUnread();

            bool functionWorked = homePage.ChangeNickname(newNickame);
            Assert.IsTrue(functionWorked);

            driver.Quit();
        }
        [Test, Order(2)]
        public void NicknameWasChanged()
        {
            driver.Url = "https://proton.me/";
            startPage.Login(email1, password1);

            homePage.OpenSettings();

            bool wasChanged = homePage.NicknameWasChanged(newNickame);
            Assert.IsTrue(wasChanged);

            driver.Quit();
        }
    }
}
