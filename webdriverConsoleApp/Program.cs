﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using WebdriverClassLib;
using WebdriverClassLib.Pages;

namespace WebdriverConsole
{
    class Program
    {
        public static void Main(string[] args)
        {
            using var driver = new ChromeDriver();
            driver.Manage().Window.Maximize();

            driver.Url = "https://proton.me/";

            var p = new StartPage(driver);



            if (p.Login("senderMailTesting@proton.me", ":bi9j3A_bXUgAvs"))
                Console.WriteLine("Login Successfull");


            var homePage = new HomePage(driver);

            if (homePage.SendEmail("receiverMailTesting@proton.me", "testTheme", RandomizeFunctions.GetRandomText(100)))
                Console.WriteLine("Send Successfull");
            
            homePage.LogOut();

            driver.Url = "https://proton.me/";

            if (p.Login("receiverMailTesting@proton.me", "ux3yRGkVc_!xqPd"))
                Console.WriteLine("Login2 Successfull");


            homePage.OpenLastUnread();


            var cont = new EmailContents(driver);

            if (cont.SendReply())
                Console.WriteLine("Reply Sent Successfuly");

            homePage.LogOut();

            driver.Url = "https://proton.me/";

            if (p.Login("senderMailTesting@proton.me", ":bi9j3A_bXUgAvs"))
                Console.WriteLine("Login3 Successfull");

            homePage.OpenLastUnread();

            string newNick = cont.GetContent();

            homePage.ChangeNickname(newNick);

            Console.WriteLine(homePage.NicknameWasChanged(newNick));

            Console.WriteLine("Ends.");
            Console.ReadLine();
            driver.Quit();
        }
    }
}