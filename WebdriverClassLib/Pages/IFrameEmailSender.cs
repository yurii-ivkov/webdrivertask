﻿using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WebdriverClassLib.Pages
{
    public class IFrameEmailSender
    {
        private readonly IWebDriver driver;
        private readonly By addresseeField = By.XPath("/html/body/div[1]/div[4]/div/div/div/div/div/div[2]/div/div/div/div/div/div/input");
        private readonly By themeField = By.XPath("/html/body/div[1]/div[4]/div/div/div/div/div/div[3]/div/div/input");
        private readonly By mesageField = By.CssSelector("#rooster-editor");

        private readonly By sendButton = By.XPath("/html/body/div[1]/div[4]/div/div/div/footer/div/div[1]/button[1]");
        private readonly By iframeEmailSender = By.XPath("/html/body/div[1]/div[4]/div/div/div/div/section/div/div[1]/div[1]/div/iframe");
        public IFrameEmailSender(IWebDriver driver)
        {
            this.driver = driver;
        }
        public bool Reply(string text)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
                var element = driver.FindElement(iframeEmailSender);

                driver.SwitchTo().Frame(element);

                element = driver.FindElement(mesageField);
                element.Clear();
                element.SendWhenInteractable(driver, TimeSpan.FromSeconds(5), text);

                driver.SwitchTo().DefaultContent();

                element = wait.Until(d => d.FindElement(sendButton));
                element.Click();

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public bool SendEmail(string addressee, string theme, string text)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));

                var element = wait.Until(d => d.FindElement(addresseeField));
                element.SendKeys(addressee);

                element = wait.Until(d => d.FindElement(themeField));
                element.SendKeys(theme);

                element = driver.FindElement(iframeEmailSender);

                driver.SwitchTo().Frame(element);

                element = driver.FindElement(mesageField);
                element.Clear();
                element.SendWhenInteractable(driver, TimeSpan.FromSeconds(5), text);

                driver.SwitchTo().DefaultContent();

                element = wait.Until(d => d.FindElement(sendButton));
                element.Click();

                Thread.Sleep(60000);

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
