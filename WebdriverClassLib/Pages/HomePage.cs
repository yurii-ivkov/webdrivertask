﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClassLib.Pages
{
    public static class WebElementExtensions
    {
        public static void SendWhenInteractable(this IWebElement element, IWebDriver driver, TimeSpan timeout, string keys)
        {
            var wait = new WebDriverWait(driver, timeout);

            wait.Until(drv =>
            {
                try
                {
                    element.SendKeys(keys);
                    return true;
                }
                catch (ElementNotInteractableException ex)
                {
                    Console.WriteLine(ex.Message);
                    return false;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return false;
                }
            });
        }
    }

    public class HomePage
    {
        private readonly IWebDriver driver;
        private readonly By newMessageButton = By.XPath("/html/body/div[1]/div[3]/div/div[2]/div/div[1]/div[3]/button");
        private readonly By unreadButton = By.XPath("/html/body/div[1]/div[3]/div/div[2]/div/div[2]/div/div/div/main/div/div/div/div/div/div[1]/div/nav/div[2]/div[1]/div/button[3]");
        private readonly By profileSpan = By.XPath("/html/body/div[1]/div[3]/div/div[2]/div/div[2]/div/div/div/header/div[2]/ul/li[3]/button");
        private readonly By logoutButton = By.XPath("//*[@id=\"dropdown-28\"]/div[2]/ul/li[5]/div/button");
        
        private readonly By settingsButton = By.XPath("/html/body/div[1]/div[3]/div/div[2]/div/div[2]/div/div/div/header/div[2]/ul/li[2]");
        private readonly By allSettingsButton = By.XPath("//*[@id=\"drawer-app-proton-settings\"]/div[2]/div/div[3]/div/div/a");
        private readonly By accountAndPassword = By.XPath("/html/body/div[1]/div[3]/div/div/div/div[1]/div[5]/nav/ul/ul[1]/li[5]/a");
        private readonly By editName = By.XPath("//*[@id=\"account\"]/div[1]/div[2]/div[2]/div/button");
        private readonly By nameField = By.CssSelector("div.settings-layout-right.pt-2 > div > div");
        private readonly By editNameField = By.XPath("//*[@id=\"displayName\"]");
        private readonly By buttonSubmit = By.XPath("/html/body/div[4]/dialog/form/div[3]/button[2]");
        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public bool SendEmail(string addressee, string theme, string text)
        {
            driver.FindElement(newMessageButton).Click();
            var sender = new IFrameEmailSender(driver);
            return sender.SendEmail(addressee, theme, text);
        }

        public bool ChangeNickname(string newNickname)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
                var element = wait.Until(d => d.FindElement(settingsButton));

                element.Click();

                wait.Until(d => d.FindElement(allSettingsButton)).Click();
                wait.Until(d => d.FindElement(accountAndPassword)).Click();
                wait.Until(d => d.FindElement(editName)).Click();

                element = wait.Until(d => d.FindElement(editNameField));
                element.SendWhenInteractable(driver, TimeSpan.FromSeconds(5), newNickname);

                wait.Until(d => d.FindElement(buttonSubmit)).Click();
                Thread.Sleep(2000);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool OpenLastUnread()
        {
            driver.FindElement(unreadButton);
            driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div[2]/div/div[2]/div/div/div/main/div/div/div/div/div/div[2]/div[2]/div[1]/div"))
                .Click();

            var a = new EmailContents(driver);

            Console.WriteLine(a.GetContent());
            Console.WriteLine(a.GetSender());

            Thread.Sleep(4000);

            return true;
        }
        public bool OpenSettings()
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
                var element = wait.Until(d => d.FindElement(settingsButton));

                element.Click();

                wait.Until(d => d.FindElement(allSettingsButton)).Click();
                wait.Until(d => d.FindElement(accountAndPassword)).Click();

                return true;
            }
            catch (Exception ex) 
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool LogOut()
        {
            driver.FindElement(profileSpan).Click();
            driver.FindElement(logoutButton).Click();

            return true;
        }

        public bool IsLoaded()
        {
            try
            {
                var element = driver.FindElement(newMessageButton);
                return element.Displayed;
            }
            catch
            {
                return false;
            }
        }
        public bool NicknameWasChanged(string expected) => driver.FindElement(nameField).Text == expected;
    }
}
