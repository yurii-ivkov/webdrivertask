﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClassLib.Pages
{
    public class EmailContents
    {
        private readonly IWebDriver driver;
        private readonly By iframeContent = By.CssSelector("div > iframe");
        private readonly By content = By.CssSelector("#proton-root > div > div > div:nth-child(1)");
        private readonly By sender = By.ClassName("message-recipient-item-label");
        private readonly By replyButton = By.CssSelector("div.pt-0.flex.flex-justify-space-between > div.button-group.button-group-outline-weak.button-group-medium.mb-2 > button:nth-child(1)");
        public EmailContents(IWebDriver driver)
        {
            this.driver = driver;
        }

        public string GetContent()
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                var element = wait.Until(d => d.FindElement(iframeContent));

                driver.SwitchTo().Frame(element);

                element = wait.Until(d => d.FindElement(content));
                string emailContent = element.Text;

                driver.SwitchTo().DefaultContent();

                return emailContent;
            }
            catch (Exception ex) 
            {
                Console.WriteLine(ex.Message);
                return "error";
            }
        }

        public string GetSender()
        {
            try
            {
                var element = driver.FindElement(sender);
                return element.Text;
            }
             catch (Exception ex) 
            {
                Console.WriteLine(ex.Message);
                return "error";
            }
        }

        public bool SendReply()
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                var element = wait.Until(d => d.FindElement(replyButton));
                element.Click();

                var emailSender = new IFrameEmailSender(driver);
                return emailSender.Reply(RandomizeFunctions.GetRandomText(10));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }

        public bool SendReply(string text)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                var element = wait.Until(d => d.FindElement(replyButton));
                element.Click();

                var emailSender = new IFrameEmailSender(driver);
                return emailSender.Reply(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }
    }
}
