﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClassLib.Pages
{
    public class StartPage
    {
        private readonly IWebDriver driver;
        private readonly By SignInButton = By.XPath("//*[@id=\"gatsby-focus-wrapper\"]/div[1]/div/div/header/div/div[2]/a[1]");
        private readonly By UsernameInput = By.CssSelector("#username");
        private readonly By PasswordInput = By.CssSelector("#password");
        private readonly By ButtonSubmit = By.XPath("/html/body/div[1]/div[4]/div[1]/main/div[1]/div[2]/form/button");

        public StartPage(IWebDriver driver)
        {
            this.driver = driver;
            Thread.Sleep(1000);
        }

        public bool Login(string email, string password)
        {
            try
            {
                driver.FindElement(SignInButton).Click();

                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(6));

                var element = wait.Until(d => d.FindElement(UsernameInput));

                
                element.SendWhenInteractable(driver, TimeSpan.FromSeconds(6), email);

                element = wait.Until(d => d.FindElement(PasswordInput));
                Console.WriteLine(element.Enabled);
                element.SendWhenInteractable(driver, TimeSpan.FromSeconds(6), password);

                element = wait.Until(d => d.FindElement(ButtonSubmit));

                element.Click();
                Thread.Sleep(8000);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
