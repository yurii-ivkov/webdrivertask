﻿using System.Text;
using System;

namespace WebdriverClassLib
{
    public static class RandomizeFunctions
    {
        private static readonly Random random = new Random();
        public static string GetRandomText(int length)
        {
            const string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var stringBuilder = new StringBuilder();

            for (int i = 0; i < length; i++)
            {
                int index = random.Next(characters.Length);
                stringBuilder.Append(characters[index]);
            }

            return stringBuilder.ToString();
        }
    }
}